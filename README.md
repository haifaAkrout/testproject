# TestProject

## Project setup
```
Download Visual Studio or any C++ IDE
Make sure that you have installed C++ support in Visual Studio
```

### Run and Build
```
To build the project, we choose Build Solution from the Build menu
To run the code, on the menu bar,we choose Debug, Start without debugging.
```

# Example
```
Size:4,4
startingPosition: 2,2
Combination via a menu:
1
4
1
3
2
3
2
4
1
0
Result: [0,1]
```
