// ProjectApp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include<vector>
#include<string>
#include <sstream>
#include "Matrix.cpp"
using namespace std;


int main()
{
	int matrixMenuIndex = 0;
	string size = "";
	string startingPosition = "";
	int x = 0;
	int y = 0;
	int width = 0;
	int height = 0;

	while (matrixMenuIndex != 3) {
		cout << "1- SET THE SIZE OF THE MATRIX" << endl;
		cout << "2- SET THE STARTING POSTION" << endl;
		cout << "3- GO TO COMBINATION" << endl;
		cout << "0- EXIT" << endl;
		cin >> matrixMenuIndex;

		switch (matrixMenuIndex) {

		case 1:
			cout << "ENTER THE MATRIX SIZE: ";
			cin >> size;
			cout << endl;
			width = int(size[0]) - '0';
			height = int(size[2]) - '0';
			break;

		case 2:
			cout << "ENTER THE STARTING POSITION: ";
			cin >> startingPosition;
			cout << endl;
			x = int(startingPosition[0]) - '0';
			y = int(startingPosition[2]) - '0';

			break;

		case 3:

			break;

		case 0:

		default:
			exit(1);
			cout << "INVALID CHOICE !" << endl;
			break;
		}
	}
	if (x > width || y > height || x<0 || y<0) {
		cout << "Object position :" << "[-1,-1]" << endl;
		exit(1);
	}
	int combinationMenuIndex = -1;
	vector<int>combination;
	while (combinationMenuIndex != 0) {
		cout << "1 = MOVE FORWARD ONE STEP" << endl;
		cout << "2 = MOVE BACKWARDS ONE STEP" << endl;
		cout << "3 = ROTATE CLOCKWISE 90 DEGREES" << endl;
		cout << "4 = ROTATE COUNTERCLOCKWISE 90 DEGREES" << endl;
		cout << "0- EXIT" << endl;
		cin >> combinationMenuIndex;
		combination.push_back(combinationMenuIndex);

	}

	Matrix matrix;
	matrix.height = height;
	matrix.width = width;
	matrix.x = x;
	matrix.y = y;
	matrix.direction = "north";

	for (int move : combination) {

		if (move == 1)
		{

			matrix.MoveForward();

			if (matrix.verifyStimulation() == false)
			{
				cout << "Object position :" << "[-1,-1]" << endl;
				exit(1);
			}

		}
		else if (move == 2)
		{
			matrix.MoveBackward();
			if (matrix.verifyStimulation() == false)
			{
				cout << "Object position :" << "[-1,-1]" << endl;
				exit(1);
			}
		}

		else if (move == 3) {
			matrix.rotateClockwise();
		}

		else if (move == 4) {
			matrix.rotateCounterClockwise();
		}
		else {
			break;
		}
	}
	cout << "End object position is : " << "[ " << matrix.x << ", " << matrix.y << " ]";


}
// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
