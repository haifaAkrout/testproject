#include <iostream>
#include<vector>
#include<string>
#include <sstream>
using namespace std;
class Matrix {
public:
	int height, width;
	int x, y;
	string direction = "north";

	bool verifyStimulation() {
		if ((x > width) || (y > height)||(x<0)|| (y<0))
		{
			return false;
		}

		return true;
	}

	void MoveForward() {
		if (direction == "north")
			y = y - 1;
		if (direction == "south")
			y = y + 1;
		if (direction == "east")
			x = x + 1;
		if (direction == "west")
			x = x - 1;

	}

	void MoveBackward() {
		if (direction == "north")
			y = y + 1;
		if (direction == "south")
			y = y - 1;
		if (direction == "east")
			x = x - 1;
		if (direction == "west")
			x = x + 1;
	}
	void rotateClockwise() {
		if (direction == "north") {
			direction = "east";
			return;
		}
		if (direction == "east") {
			direction = "south";
			return;
		}
		if (direction == "south") {
			direction = "west";
			return;
		}
		if (direction == "west") {
			direction = "north";
			return;
		}
	}

	void rotateCounterClockwise() {
		if (direction == "north")
		{
			direction = "west";
			return;
		}
		if (direction == "east") {
			direction = "north";
			return;
		}
		if (direction == "south")
		{
			direction = "east";
			return;
		}
		if (direction == "west") {
			direction = "south";
			return;
		}

	}

};